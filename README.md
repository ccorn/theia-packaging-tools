# Theia packaging tools

This repository contains helpers for packaging releases of `electron`-based
builds of the [Theia IDE].

The main tool currently is a `bash` script `make-package-json.sh` which
outputs contents for a `package.json` suitable for building the `electron`
variant of Theia.
The script looks up the required data at the official [Theia repository]
and does not require any local files except its own optional configuration.

`make-package-json.sh` has been created to automate the steps detailed
in the [Upgrade Guide] by Bence Hornák and Leon De Andrade who also
have provided manually crafted versions of conforming `package.json`
files for the Arch User Repository (AUR) package [theia-electron][2].

## Basic usage

    ./make-package-json.sh --help           # for documentation
    ./make-package-json.sh >package.json    # autodetects latest stable release
    ./make-package-json.sh 1.17.2 >package.json
    ./make-package-json.sh 1.18.0-next.1f1aecb5 >package.json
    theia_next=$(./get-latest-version.sh --next "@theia/core")
    ./make-package-json.sh $theia_next >package.json

`make-package-json.sh` also reads `extra-plugins.json` where you can list
plugins to add, remove, or freeze at a given version. Useful example included.

The script does a lot of checking to detect unintended results, so you
are likely to get a warning if upstream things change in unexpected ways.

Once you have generated the `package.json`, building `theia-electron`
can be done by:

    yarn install
    yarn build

More refined build steps, including the packaging, can be found by checking out
the files at the [AUR packaging page][2] or its [source repository][1].

## Additional tools

### Querying the latest snapshot version

`get-latest-version.sh` can be used to query the latest version of a specified
package from the NPM registry, optionally enabling `*-next.*` versions.
Use the `-h` option for usage information.

## Requirements

  * `bash >= 5` (the script uses some advanced `bash` features)
  * GNU `coreutils` and `diffutils`
  * `curl` for HTTPS requests to the Github or NPM API
  * [`jq`][jq] for transforming JSON data
  * `yarn`, `node-gyp`, `typescript` for subsequent building of Theia.
    Not needed by the helper scripts described here.

## License

`make-package-json.sh` and `get-latest-version.sh` are licensed
under the terms of the [MIT license](LICENSE.md).

[1]: https://gitlab.com/bencehornak/arch-theia-electron
[2]: https://aur.archlinux.org/packages/theia-electron/
[jq]: https://stedolan.github.io/jq/
[Theia IDE]: https://theia-ide.org/
[Theia repository]: https://github.com/eclipse-theia/theia
[Upgrade Guide]: https://gitlab.com/bencehornak/arch-theia-electron/-/wikis/Upgrade-guide

