#! /bin/bash

# MIT License
#
# Copyright (c) 2021 Christian Cornelssen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

me=${0##*/}

usage='SYNOPSIS

    '"$me"' [OPTIONS] PACKAGE

Output (to stdout) the latest version of PACKAGE available at the NPM registry,
optionally enabling *-next.* versions. Requires curl and jq.

OPTIONS

  -h, --help
    Output this usage information and exit.

  -N, --next
    Output latest *-next.* version if any, otherwise the latest release.
    Without this option, the order is reversed: Prefer the latest release
    and fall back to the most recent *-next.* version if there is no release.

  -v LEVEL, --verbosity=LEVEL
    Set the verbosity level for messages to stderr. LEVEL can be:

      * error: Only output errors
      * warn: Also output warnings
      * info: Also output progress information
      * debug: Output gory details

    The default verbosity level is "info".

EXAMPLE

    '"$me"' "@theia/core"

EXIT STATUS

Nonzero in case of errors, zero otherwise.'

set -o pipefail
shopt -s lastpipe
((errors=0))
verbosity=info
version_order=.latest//.next
version_spec=""

log() {
  echo >&2 "// $me:" "$@"
}

debug() {
  case $verbosity in (debug) log "Debug:" "$@" ;; esac
}

info() {
  case $verbosity in (debug|info) log "Info:" "$@" ;; esac
}

warn() {
  case $verbosity in (debug|info|warn) log "Warning:" "$@" ;; esac
}

error() {
  log "Error:" "$@"
  ((++errors))
}

fatal() {
  log "Fatal:" "$@"
  exit 1
}

have() {
  type -p "$1" >/dev/null
}

get() {
  debug GET "${@:$#}"
  curl -LsS "$@"
}

registry_url=https://registry.npmjs.org/
accept_header="Accept: application/vnd.npm.install-v1+json"
query_npm() {   # package-name
  local package_escaped package_url
  package_escaped=$(jq -nr --arg pkg "$1" '$pkg|@uri')
  package_url=$registry_url$package_escaped
  get -H "$accept_header" "$package_url"
}

# Handle options
while [ $# -gt 0 ]; do
  case $1 in
  (-h|--help)
    echo "$usage"
    exit ;;
  (--verbosity=*)
    verbosity=${1#*=}
    shift ;;
  (-v|--verbosity)
    [ $# -gt 1 ] || fatal "Option $1 requires an argument"
    shift
    verbosity=$1
    shift ;;
  (-N|--next)
    version_order=.next//.latest
    version_spec=", including *-next.*"
    shift ;;
  (--)
    shift ;;
  (-?*)
    fatal "Unknown option ${1@Q}" ;;
  (*) break ;;
  esac
done

case $verbosity in
(error|warn|info|debug) ;;
(*) fatal "Unknown verbosity ${verbosity@Q}" ;;
esac

# Package to check
[ $# = 1 ] || fatal "Exactly one package name required. Use -h for help."
package=$1

# Test for tools
have jq         || fatal "Command jq not found."
have curl       || fatal "Command curl not found."

info "Query latest version of $package$version_spec"
latest_version=$(query_npm "$package" \
| jq -re '.["dist-tags"]|'"$version_order") \
|| fatal "Cannot query latest version of $package"
info "Latest version of $package: $latest_version"

# Output result to stdout
printf '%s\n' "$latest_version" || error "error writing to stdout"

# Exit with appropriate status
((!errors))

